package com.son.loginmain;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.app.AlertDialog;


public class giaodien extends Activity {
    AlertDialog.Builder adb;
    Button btnquiz, btncorret;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.giao_dien);
        btnquiz =   (Button)findViewById(R.id.btn_trac_nghiem);
        btncorret = (Button)findViewById(R.id.btn_corretword);

        btnquiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(giaodien.this, tracnghiem.class);
                startActivity(intent);
            }


        });

        adb= new AlertDialog.Builder(this);
        btncorret.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(giaodien.this, corretword.class);
                startActivity(intent);
            }


        });




    }
    public void quit(View view)
    {

        this.onBackPressed();
    }

    public void how(View view){
        adb.setTitle("Guide play:");
        adb.setMessage("Read the question and choose 1 of the 4 answers below to fill in the blanks.");

        adb.setPositiveButton("OK", null);
        adb.show();
    }
    public void how1(View view){
        adb.setTitle("Guide play:");
        adb.setMessage("Read and rearrange the letters on the screen to make a meaningful word.");

        adb.setPositiveButton("OK", null);
        adb.show();
    }

}

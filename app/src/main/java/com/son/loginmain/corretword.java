package com.son.loginmain;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class corretword extends AppCompatActivity {
    EditText et;
    String string;
    Intent intent;
    Button quit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.corret_word);
        quit = findViewById(R.id.quit);
        et= findViewById(R.id.editText1);
    }

    public void moveToNextActivity(View v){
        intent=new Intent("com.correctword.levelselect");
        string=et.getText().toString();
        if(string.equals(""))
        {
            Toast.makeText(corretword.this,"Please enter your Name", Toast.LENGTH_LONG).show();
            et.setText("");
        }
        else{
            intent.putExtra("name",et.getText().toString());
            startActivity(intent);
        }
    }
    public void quit(View view)
    {

        this.onBackPressed();
    }
}

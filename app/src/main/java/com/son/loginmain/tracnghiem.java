package com.son.loginmain;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class tracnghiem extends AppCompatActivity implements View.OnClickListener{
    private TextView tvQuestion;
    private TextView tvContentQuestion;
    private TextView tvAnswer1,tvAnswer2,tvAnswer3,tvAnswer4;
    Button quit;
    int score=0;
    TextView points;


    private List<Question> mListQuestion;
    private Question mQuestion;
    private int currentQuestion =0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trac_nghiem);
        quit = findViewById(R.id.quit);
        points=(TextView) findViewById(R.id.score);


        initUi();

        mListQuestion = getListQuestion();
        if(mListQuestion.isEmpty())
        {
            return;
        }
        setDataQuestion(mListQuestion.get(currentQuestion));

    }
    private void setDataQuestion(Question question)
    {
        if(question == null){
            return;
        }

        mQuestion = question;

        tvAnswer1.setBackgroundResource(R.drawable.bg_corner_blue_30);
        tvAnswer2.setBackgroundResource(R.drawable.bg_corner_blue_30);
        tvAnswer3.setBackgroundResource(R.drawable.bg_corner_blue_30);
        tvAnswer4.setBackgroundResource(R.drawable.bg_corner_blue_30);





        String titleQuestion = "Question " + question.getNumber();
        tvQuestion.setText(titleQuestion);
        tvContentQuestion.setText(question.getContent());
        tvAnswer1.setText(question.getListAnswer().get(0).getContent());
        tvAnswer2.setText(question.getListAnswer().get(1).getContent());
        tvAnswer3.setText(question.getListAnswer().get(2).getContent());
        tvAnswer4.setText(question.getListAnswer().get(3).getContent());

        tvAnswer1.setOnClickListener((View.OnClickListener)this);
        tvAnswer2.setOnClickListener((View.OnClickListener) this);
        tvAnswer3.setOnClickListener((View.OnClickListener) this);
        tvAnswer4.setOnClickListener((View.OnClickListener)this);



    }

    private void initUi()
    {
        tvQuestion = findViewById(R.id.tv_question);
        tvContentQuestion = findViewById(R.id.tv_content_question);
        tvAnswer1 = findViewById(R.id.tv_answer1);
        tvAnswer2 = findViewById(R.id.tv_answer2);
        tvAnswer3 = findViewById(R.id.tv_answer3);
        tvAnswer4 = findViewById(R.id.tv_answer4);


    }
    private List<Question> getListQuestion()
    {
        List<Question> list = new ArrayList<>();
        List<Answer> answersList1 = new ArrayList<>();
        answersList1.add(new Answer(  "This", false));

        answersList1.add(new Answer("Them", false));
        answersList1.add(new Answer(" That", false));
                answersList1.add(new Answer("Those", true));

        List<Answer> answersList2 = new ArrayList<>();
        answersList2.add(new Answer(  "Frenchman", false));
        answersList2.add(new Answer("A French", true));
        answersList2.add(new Answer("A Frenchman", false));
        answersList2.add(new Answer("French man", false));

        List<Answer> answersList3 = new ArrayList<>();
        answersList3.add(new Answer(  "Buy", false));
        answersList3.add(new Answer("Going to buy", false));
        answersList3.add(new Answer("Am buying", true));
        answersList3.add(new Answer("Bought", false));

        List<Answer> answersList4 = new ArrayList<>();
        answersList4.add(new Answer(  "Getting", true));
        answersList4.add(new Answer("Going ", false));
        answersList4.add(new Answer("Doing ", false));
        answersList4.add(new Answer("Putting", false));

        list.add(new Question(  "Who are all ________ people?", 1 , answersList1));
        list.add(new Question(  "Claude is ________.", 2 , answersList2));
        list.add(new Question(  "I ____ a car next year", 3 , answersList3));
        list.add(new Question(  "They are all ________ ready for the party", 4 , answersList4));

        List<Answer> answersList5 = new ArrayList<>();
        answersList5.add(new Answer(  "playing", true));
        answersList5.add(new Answer("doing", false));
        answersList5.add(new Answer("studying", false));
        answersList5.add(new Answer("having", false));

        list.add(new Question(  "I often spend the weekend _____________ badminton with my elder brother.", 5 , answersList5));

        List<Answer> answersList6 = new ArrayList<>();
        answersList6.add(new Answer(  "have", false));
        answersList6.add(new Answer("is having", false));
        answersList6.add(new Answer("has", true));
        answersList6.add(new Answer("had", false));

        list.add(new Question(  "Clara is a _____________ girl. She doesn’t talk much when she meets new friends.", 6 , answersList6));

        List<Answer> answersList7 = new ArrayList<>();
        answersList7.add(new Answer(  "talkative", false));
        answersList7.add(new Answer("reserved", true));
        answersList7.add(new Answer("active", false));
        answersList7.add(new Answer("confident", false));
        list.add(new Question(  "Clara is a _____________ girl. She doesn’t talk much when she meets new friends.", 7 , answersList7));

        List<Answer> answersList8 = new ArrayList<>();
        answersList8.add(new Answer(  "on", false));
        answersList8.add(new Answer("behind", false));
        answersList8.add(new Answer("between", true));
        answersList8.add(new Answer("under", false));

        list.add(new Question(  "Where is the cat? – It’s _____________ the table and the bookshelf.", 8 , answersList8));

        List<Answer> answersList9 = new ArrayList<>();
        answersList9.add(new Answer(  "finish", false));
        answersList9.add(new Answer("finishing", false));
        answersList9.add(new Answer("finished", false));
        answersList9.add(new Answer("finishes", true));

        list.add(new Question(  "Alex usually ____________ his homework at 8 p.m.", 9, answersList9));

        List<Answer> answersList10 = new ArrayList<>();
        answersList10.add(new Answer(  "are", true));
        answersList10.add(new Answer("is", false));
        answersList10.add(new Answer("was", false));
        answersList10.add(new Answer("being", false));

        list.add(new Question(  "There ____________ two pencils on the desk", 10 , answersList10));

        List<Answer> answersList11 = new ArrayList<>();
        answersList11.add(new Answer(  "have", false));
        answersList11.add(new Answer("talk", false));
        answersList11.add(new Answer("talk", false));
        answersList11.add(new Answer("do", true));

        list.add(new Question(  "You should _________ physical exercise regularly.", 11 , answersList11));

        List<Answer> answersList12 = new ArrayList<>();
        answersList12.add(new Answer(  "Does/do", true));
        answersList12.add(new Answer("Is/do", false));
        answersList12.add(new Answer("Does/doing", false));
        answersList12.add(new Answer("Is/does", false));

        list.add(new Question(  "____________ Bobby often ____________ physics experiment after school?", 12, answersList12));

        List<Answer> answersList13 = new ArrayList<>();
        answersList13.add(new Answer(  "go", true));
        answersList13.add(new Answer("have", false));
        answersList13.add(new Answer("make", false));
        answersList13.add(new Answer("take", false));

        list.add(new Question(  "We _________ to the judo club twice a month.", 13 , answersList13));

        List<Answer> answersList14 = new ArrayList<>();
        answersList14.add(new Answer(  "explain", false));
        answersList14.add(new Answer("is explaining", true));
        answersList14.add(new Answer("explains", false));
        answersList14.add(new Answer("to explain", false));

        list.add(new Question(  "Listen! The teacher __________ the lesson to us.", 14 , answersList14));

        List<Answer> answersList15 = new ArrayList<>();
        answersList15.add(new Answer(  "is reading", false));
        answersList15.add(new Answer("to read", false));
        answersList15.add(new Answer("reads", false));
        answersList15.add(new Answer("read", true));

        list.add(new Question(  "What do you do in your free time? – I always ________ books in my free time.", 15 , answersList15));

        List<Answer> answersList16 = new ArrayList<>();
        answersList16.add(new Answer(  "piece of cake", false));
        answersList16.add(new Answer("sweets and candy  ", false));
        answersList16.add(new Answer("biscuit", false));
        answersList16.add(new Answer("cup of tea", true));

        list.add(new Question(  "I’ve never really enjoyed going to the ballet or the opera; they’re not really my ________.", 16 , answersList16));

        List<Answer> answersList17 = new ArrayList<>();
        answersList17.add(new Answer(  "uncontrolled ", false));
        answersList17.add(new Answer("arranged  ", false));
        answersList17.add(new Answer("chaotic ", true));
        answersList17.add(new Answer("notorious", false));

        list.add(new Question(  "The 1st week of classes at university is a little ______ because so many students get lost, change classes or go to the wrong place.", 17 , answersList17));

        List<Answer> answersList18 = new ArrayList<>();
        answersList18.add(new Answer(  "last", true));
        answersList18.add(new Answer("the end", false));
        answersList18.add(new Answer("present", false));
        answersList18.add(new Answer("the moment", false));

        list.add(new Question(  ": He has been waiting for this letter for days, and at ________ it has come.", 18, answersList18));

        List<Answer> answersList19 = new ArrayList<>();
        answersList19.add(new Answer( "self-conscious", true));
        answersList19.add(new Answer("self-satisfied", false));
        answersList19.add(new Answer("self-directed", false));
        answersList19.add(new Answer("self-confident", false));

        list.add(new Question(  "Paul is a very _______ character, he is never relaxed with strangers.", 19 , answersList19));

        List<Answer> answersList20 = new ArrayList<>();
        answersList20.add(new Answer("put up with", true));
        answersList20.add(new Answer("catch up with", false));
        answersList20.add(new Answer("keep up with", false));
        answersList20.add(new Answer("come down with", false));


        list.add(new Question(  "Although he is my friend, I find it hard to _______ his selfishness", 21 , answersList20));

        List<Answer> answersList21 = new ArrayList<>();
        answersList21.add(new Answer( "take a fancy to ", false));
        answersList21.add(new Answer("keep an eye on", false));
        answersList21.add(new Answer("get a kick out of     ", true));
        answersList21.add(new Answer("kick up a fuss about", false));

        list.add(new Question(  "I used to ______ reading comics, but now I've grown out of it.", 21 , answersList21));

        List<Answer> answersList23 = new ArrayList<>();
        answersList23.add(new Answer(  "to", true));
        answersList23.add(new Answer("to the", false));
        answersList23.add(new Answer("in", false));
        answersList23.add(new Answer("in the", false));

        list.add(new Question(  "When do you go ________ bed?.", 23 , answersList23));

        List<Answer> answersList24 = new ArrayList<>();
        answersList24.add(new Answer(  "its", true));
        answersList24.add(new Answer("it is", false));
        answersList24.add(new Answer("it", false));
        answersList24.add(new Answer("is", false));

        list.add(new Question(  "London is famous for _____ red buses", 24 , answersList24));

        List<Answer> answersList25 = new ArrayList<>();
        answersList25.add(new Answer(  "many", false));
        answersList25.add(new Answer("a lot", false));
        answersList25.add(new Answer("some", true));
        answersList25.add(new Answer("much", false));

        list.add(new Question(  "Is there _____ milk in the fridge?", 25 , answersList25));
        List<Answer> answersList26 = new ArrayList<>();
        answersList26.add(new Answer(  "had arrived / discovered / prepared", false));
        answersList26.add(new Answer("was arriving / had discovered / was preparing", false));
        answersList26.add(new Answer("arrived / discovered / was preparing", true));
        answersList26.add(new Answer("have arrived / was discovering / had prepared", false));

        list.add(new Question(  "When I ……….home last night, I ……. that Jane …… a beautiful candlelight dinner", 26 , answersList26));

        List<Answer> answersList27 = new ArrayList<>();
        answersList27.add(new Answer(  "might have been", false));
        answersList27.add(new Answer(" should have been", false));
        answersList27.add(new Answer("mustn’t have been", true));
        answersList27.add(new Answer("shouldn’t have been", false));

        list.add(new Question(  "He_________________ the party tonight as he is on business in Paris now.", 27, answersList27));
        List<Answer> answersList28 = new ArrayList<>();
        answersList28.add(new Answer(  "the/ a", false));
        answersList28.add(new Answer(" Ø/ Ø", false));
        answersList28.add(new Answer("Ø/ a", true));
        answersList28.add(new Answer("Ø/ the", false));

        list.add(new Question(  "Mary really fancies _____________ detective books, _________ genre of book about excellent detectives and cruel criminals.",28, answersList28));

        List<Answer> answersList29 = new ArrayList<>();
        answersList29.add(new Answer(  "Detective books", false));
        answersList29.add(new Answer(" Horror films", false));
        answersList29.add(new Answer("Biography", false)); answersList29.add(new Answer("Documentary", true));

        list.add(new Question(  "_____________ is a film or TV program presenting the facts about a person or event..",29 ,answersList29));

        List<Answer> answersList30 = new ArrayList<>();
        answersList30.add(new Answer(  "the", true));
        answersList30.add(new Answer(" you", false));
        answersList30.add(new Answer("a", false));
        answersList30.add(new Answer("an", false));

        list.add(new Question(  "Where are _____ children? – They go to school .",30 ,answersList30));



        return list;
    }

    @Override

    public void onClick(View v) {

        switch (v.getId()){
            case R.id.tv_answer1:
                tvAnswer1.setBackgroundResource(R.drawable.bg_corner_orange_30);
                checkAnswer(tvAnswer1, mQuestion, mQuestion.getListAnswer().get(0));
                break;

            case R.id.tv_answer2:
                tvAnswer2.setBackgroundResource(R.drawable.bg_corner_orange_30);
                checkAnswer(tvAnswer2, mQuestion, mQuestion.getListAnswer().get(1));
                break;

            case R.id.tv_answer3:
                tvAnswer3.setBackgroundResource(R.drawable.bg_corner_orange_30);
                checkAnswer(tvAnswer3, mQuestion, mQuestion.getListAnswer().get(2));
                break;

            case R.id.tv_answer4:
                tvAnswer4.setBackgroundResource(R.drawable.bg_corner_orange_30);
                checkAnswer(tvAnswer4, mQuestion, mQuestion.getListAnswer().get(3));
                break;
        }

    }

    private void checkAnswer(final TextView textView, Question question, final Answer answer)
    {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(answer.isCorrect()){
                    textView.setBackgroundResource(R.drawable.bg_corner_green_30);
                    showdiem(question);
                    points.setText("Correct:  "+score);
                    nextQuestion();


                }else {
                    textView.setBackgroundResource(R.drawable.bg_corner_red_30);
                    showAnswerCorrect(question);
                    nextQuestion();
                    gameOver();
                }

            }
        }, 1000);
    }
    private void showdiem(Question question) {

        if (question.getListAnswer().get(0).isCorrect() ) {
            score++;


        }else {
            score++;

        }

    }



    private void showAnswerCorrect(Question question) {
        if (question == null  || question.getListAnswer()==null || question.getListAnswer().isEmpty())
        {
            return;
        }
        if (question.getListAnswer().get(0).isCorrect() )
        {
            tvAnswer1.setBackgroundResource(R.drawable.bg_corner_green_30);

        }else if (question.getListAnswer().get(1).isCorrect()){
            tvAnswer2.setBackgroundResource(R.drawable.bg_corner_green_30);

        }else if (question.getListAnswer().get(2).isCorrect()){
            tvAnswer3.setBackgroundResource(R.drawable.bg_corner_green_30);

        }else if (question.getListAnswer().get(3).isCorrect()){
            tvAnswer4.setBackgroundResource(R.drawable.bg_corner_green_30);
        }
    }
    private void gameOver()
    {





            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    showDialog("You Lose");
                    showDialog("Point: " + score);


                }
            }, 1000);

        }





    private void nextQuestion() {
        if(currentQuestion == mListQuestion.size()-1)
        {
            showDialog("You Win");
            showDialog("Point: " + score);

        }else {
            currentQuestion++;

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    setDataQuestion(mListQuestion.get(currentQuestion));


                }
            }, 1000);

        }
    }





    private void showDialog(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setCancelable(false);

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                currentQuestion = 0;
                setDataQuestion(mListQuestion.get(currentQuestion));

                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
    public void quit(View view)
    {

        this.onBackPressed();
    }





}
